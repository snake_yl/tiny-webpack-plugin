/*
 * @Author: Yang Lin
 * @Description: 简介
 * @Date: 2020-10-30 16:44:36
 * @LastEditTime: 2020-12-01 21:18:21
 * @FilePath: f:\sourcecode\tiny-webpack-plugin\example\index.js
 */
import pic1 from './assets/pic1.jpg';
import pic2 from './assets/pic2.jpg';
import pic3 from './assets/pic3.jpg';
import pic4 from './assets/pic4.jpg';
import pic5 from './assets/pic5.svg';
import pic6 from './assets/pic6.png';
import pic7 from './assets/pic7.jpg';
import pic8 from './assets/pic8.jpg';
import pic9 from './assets/pic9.jpg';
import pic10 from './assets/pic10.jpg';
import pic11 from './assets/pic11.png';
import pic12 from './assets/pic12.jpg';
import pic13 from './assets/pic13.jpg';
import pic14 from './assets/pic14.jpg';
import pic15 from './assets/pic15.jpg';
import pic16 from './assets/pic16.jpg';
import pic17 from './assets/pic17.jpg';
import pic18 from './assets/pic18.jpg';
import pic19 from './assets/pic19.jpg';
import pic20 from './assets/pic20.jpg';
import pic21 from './assets/pic21.jpg';
import pic22 from './assets/pic22.jpg';
import pic23 from './assets/pic23.jpg';
import pic24 from './assets/pic24.png';
import pic25 from './assets/pic25.jpg';
import pic26 from './assets/pic26.jpg';
import pic27 from './assets/pic27.jpg';
import pic28 from './assets/pic28.png';
import pic29 from './assets/pic29.png';
import pic30 from './assets/pic30.png';
import pic31 from './assets/pic31.png';
import pic32 from './assets/pic32.png';
import pic33 from './assets/pic33.png';
import pic34 from './assets/pic34.png';
import pic35 from './assets/pic35.png';
import pic36 from './assets/pic36.png';

function loadImg(src){
    const img = new Image();
    img.onload = function(){
        document.body.appendChild(img);
    };
    img.src = src;
}

[
    pic1,
    pic2,
    pic3,
    pic4,
    pic5,
    pic6,
    pic7,
    pic8,
    pic9,
    pic10,
    pic11,
    pic12,
    pic13,
    pic14,
    pic15,
    pic16,
    pic17,
    pic18,
    pic19,
    pic20,
    pic21,
    pic22,
    pic23,
    pic24,
    pic25,
    pic26,
    pic27,
    pic28,
    pic29,
    pic30,
    pic31,
    pic32,
    pic33,
    pic34,
    pic35,
    pic36,
].forEach(src => loadImg(src));
